package com.example.myfirstapp.di

import com.example.analytics.core.Analytics
import com.example.analytics.core.IAnalytics
import com.example.myfirstapp.list.FruitsListAdapter
import com.example.myfirstapp.list.FruitsListViewModel
import com.example.myfirstapp.util.IPriceConversionUtil
import com.example.myfirstapp.util.IWeightConversionUtil
import com.example.myfirstapp.util.PriceConversionUtil
import com.example.myfirstapp.util.WeightConversionUtil
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val homeModule = module {
    viewModel { FruitsListViewModel() }
    single { FruitsListAdapter() }
}

val utilsModule = module {
    single<IPriceConversionUtil> { PriceConversionUtil() }
    single<IWeightConversionUtil> { WeightConversionUtil() }
}

val analyticsModule = module {
    single<IAnalytics> { Analytics() }
}