package com.example.myfirstapp.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import coil.api.load
import coil.transform.RoundedCornersTransformation
import com.example.analytics.core.AnalyticsType
import com.example.analytics.core.IAnalytics
import com.example.myfirstapp.R
import com.example.myfirstapp.dto.Fruit
import com.example.myfirstapp.util.IPriceConversionUtil
import com.example.myfirstapp.util.IWeightConversionUtil
import kotlinx.android.synthetic.main.fragment_fruits_detail.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import java.util.*

class FruitsDetailFragment : Fragment() {

    private val TAG = javaClass.simpleName

    private lateinit var fruit: Fruit

    private var screenCreateTimeInMillis: Long = 0
    private var screenShownTimeInMillis: Long = 0

    // helper classes
    private val priceConversionUtil: IPriceConversionUtil by inject()
    private val weightConversionUtil: IWeightConversionUtil by inject()
    private val analytics: IAnalytics by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        screenCreateTimeInMillis = System.currentTimeMillis()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_fruits_detail, container, false)
    }

    @ExperimentalStdlibApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let {
            fruit = it.getParcelable(getString(R.string.args_fruit_item)) ?: return@let
            tvFruitName.text = fruit.type.capitalize(Locale.getDefault())
            val price =
                String.format(
                    getString(R.string.text_formatted_price),
                    priceConversionUtil.getPriceInPence(fruit.price)
                )
            tvFruitPrice.text = price
            val weight =
                String.format(
                    getString(R.string.text_formatted_weight),
                    weightConversionUtil.getWeightInKg(fruit.weight)
                )
            tvFruitWeight.text = weight
        }

        // back button
        imgBackButton.setOnClickListener { Navigation.findNavController(it).navigateUp() }

        // cover image
        imgFruitImage.load(R.drawable.ic_fruits) {
            crossfade(true)
            placeholder(R.drawable.ic_fruits)
            error(R.drawable.ic_baseline_broken_image_24)
            transformations(RoundedCornersTransformation())
        }
    }

    override fun onResume() {
        super.onResume()
        screenShownTimeInMillis = System.currentTimeMillis()
        val screenBootTime = screenShownTimeInMillis - screenCreateTimeInMillis
        GlobalScope.launch { analytics.upload(AnalyticsType.SCREEN_TIME, screenBootTime) }
    }
}