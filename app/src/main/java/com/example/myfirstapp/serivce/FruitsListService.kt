package com.example.myfirstapp.serivce

import com.example.myfirstapp.dto.FruitsResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET

/**
 * An interface consists of APIs
 */
interface FruitsListService {

    /**
     * API to fetch the Fruits list from remote using API. The sample API call would be
     * https://raw.githubusercontent.com/fmtvp/recruit-test-data/master/data.json.
     *
     * @return list of Fruits in [FruitsResponse] format.
     */
    @GET("data.json")
    fun getRemoteFruitsListAsync(): Deferred<Response<FruitsResponse>>
}
