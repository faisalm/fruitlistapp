package com.example.myfirstapp.serivce

import com.example.analytics.service.Constants
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitFactory {

    private val TAG = javaClass.simpleName

    fun create(): FruitsListService {
        // create an RxJava Adapter, network calls made asynchronous
        val rxAdapter = CoroutineCallAdapterFactory.invoke()
        // http interceptor
        val httpClient = prepareHttpInterceptor()
        // retrofit object
        val retrofit = prepareRetrofitInstance(httpClient, rxAdapter)
        return retrofit.create(FruitsListService::class.java)
    }

    /**
     *
     */
    private fun prepareRetrofitInstance(
        httpClient: OkHttpClient,
        callAdapterFactory: CoroutineCallAdapterFactory
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(Constants.BASE_API)
            .client(httpClient)
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(callAdapterFactory)
            .build()
    }

    private val loggingInterceptor = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }

    /**
     *
     */
    private fun prepareHttpInterceptor(): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .build()
    }
}
