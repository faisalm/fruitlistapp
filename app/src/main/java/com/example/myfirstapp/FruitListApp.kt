package com.example.myfirstapp

import android.app.Application
import android.os.StrictMode
import com.example.myfirstapp.di.analyticsModule
import com.example.myfirstapp.di.homeModule
import com.example.myfirstapp.di.utilsModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class FruitListApp : Application() {

    override fun onCreate() {
        super.onCreate()

        // strict mode - DEBUG(only)
        setStrictMode()

        // dependency injection configuration
        injectDependencies()
    }

    /**
     * Strict mode to analyze the main thread, only meant for DEBUG build.
     */
    private fun setStrictMode() {
        if (BuildConfig.DEBUG) {
            StrictMode.setThreadPolicy(
                StrictMode.ThreadPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
                    .build()
            )
            StrictMode.setVmPolicy(
                StrictMode.VmPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
                    .build()
            )
        }
    }

    /**
     * dependency injection initialisation.
     */
    private fun injectDependencies() {
        startKoin {
            androidLogger(Level.DEBUG)
            androidContext(this@FruitListApp)
            modules(
                listOf(
                    homeModule,
                    utilsModule,
                    analyticsModule
                )
            )
        }
    }
}