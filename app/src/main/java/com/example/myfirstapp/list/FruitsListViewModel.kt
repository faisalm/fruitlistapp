package com.example.myfirstapp.list

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.analytics.core.AnalyticsType
import com.example.analytics.core.IAnalytics
import com.example.myfirstapp.dto.FruitsResponse
import com.example.myfirstapp.serivce.RetrofitFactory
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.koin.core.KoinComponent
import org.koin.core.inject

class FruitsListViewModel : ViewModel(), KoinComponent {

    private val TAG = javaClass.simpleName

    private val fruitsAPIService = RetrofitFactory.create()

    // helper classes
    private val analytics: IAnalytics by inject()

    val fruitsListLiveData = MutableLiveData<FruitsResponse>()

    init {
        getFruitsList()
    }

    fun getFruitsList() {
        GlobalScope.launch {
            val fruitsListRequest = fruitsAPIService.getRemoteFruitsListAsync()
            try {
                val response = fruitsListRequest.await()
                if (response.isSuccessful) {
                    fruitsListLiveData.postValue(response.body())

                    // analytics - network time-laps
                    val requestTime = response.raw().sentRequestAtMillis
                    val responseTime = response.raw().receivedResponseAtMillis
                    val apiLoadTime = responseTime.minus(requestTime)
                    analytics.upload(AnalyticsType.NETWORK_LOAD, apiLoadTime)
                } else {
                    // analytics - error
                    val data = "${response.errorBody()}".replace(" ", "%20")
                    analytics.upload(AnalyticsType.APP_CRASH, data)
                }
            } catch (e: Exception) {
                // analytics - exception
                val data = "${e.cause} ${e.stackTrace[0].lineNumber}".replace(" ", "%20")
                analytics.upload(AnalyticsType.APP_CRASH, data)
            }
        }
    }
}
