package com.example.myfirstapp.list

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import coil.transform.RoundedCornersTransformation
import com.example.myfirstapp.R
import com.example.myfirstapp.dto.Fruit
import kotlinx.android.synthetic.main.layout_fruits_list_item.view.*
import org.koin.core.KoinComponent
import org.koin.core.inject
import java.util.*
import kotlin.collections.ArrayList

class FruitsListAdapter : RecyclerView.Adapter<FruitsListAdapter.ViewHolder>(), KoinComponent {

    private val TAG = javaClass.simpleName

    private val context: Context by inject()

    private var oldUserList = ArrayList<Fruit>()

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun getItemCount() = oldUserList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(
            R.layout.layout_fruits_list_item,
            parent,
            false
        ) as View
        return ViewHolder(view)
    }

    @ExperimentalStdlibApi
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val fruit = oldUserList[position]

        // view furnishing
        holder.view.imgFruitThumbnail.load(R.drawable.ic_fruits) {
            crossfade(true)
            placeholder(R.drawable.ic_fruits)
            error(R.drawable.ic_baseline_broken_image_24)
            transformations(RoundedCornersTransformation())
        }
        holder.view.tvFruitItem.text = fruit.type.capitalize(Locale.getDefault())

        // navigate to details page
        holder.view.setOnClickListener {
            val bundle = Bundle()
            bundle.putParcelable(context.getString(R.string.args_fruit_item), fruit)
            Navigation.findNavController(it)
                .navigate(R.id.action_fruitsListFragment_to_fruitsDetailFragment, bundle)
        }
    }

    /**
     * Updates the data sets and notifies the adapter.
     *
     * @param newUserList supplied to replace with populated list.
     */
    fun updateAdapter(newUserList: List<Fruit>) {
        Log.d(TAG, "updateAdapter - list size: ${newUserList.size}")
        val diffCallback = FruitsListDiffCallback(oldUserList, newUserList)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        oldUserList.clear()
        oldUserList.addAll(newUserList)
        diffResult.dispatchUpdatesTo(this)
        notifyDataSetChanged()
    }
}
