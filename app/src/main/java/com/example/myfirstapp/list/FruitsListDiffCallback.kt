package com.example.myfirstapp.list

import androidx.recyclerview.widget.DiffUtil
import com.example.myfirstapp.dto.Fruit

class FruitsListDiffCallback(
    private val mOldList: List<Fruit>,
    private val mNewList: List<Fruit>
) :
    DiffUtil.Callback() {

    override fun getOldListSize() = mOldList.size

    override fun getNewListSize() = mNewList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
        mOldList[oldItemPosition].type == mNewList[newItemPosition].type

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
        mOldList[oldItemPosition].type == mNewList[newItemPosition].type
}
