package com.example.myfirstapp.list

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.analytics.core.AnalyticsType
import com.example.analytics.core.IAnalytics
import com.example.myfirstapp.R
import com.example.myfirstapp.dto.Fruit
import com.example.myfirstapp.dto.FruitsResponse
import kotlinx.android.synthetic.main.fragment_fruits_list.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class FruitsListFragment : Fragment() {

    private val TAG = javaClass.simpleName

    private var screenCreateTimeInMillis: Long = 0
    private var screenShownTimeInMillis: Long = 0

    // helper classes
    private val viewModel: FruitsListViewModel by viewModel()
    private val mAdapter: FruitsListAdapter by inject()
    private val analytics: IAnalytics by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        screenCreateTimeInMillis = System.currentTimeMillis()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_fruits_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // recycle-view
        rvFruitsList.apply {
            layoutManager = LinearLayoutManager(context) as RecyclerView.LayoutManager?
            adapter = mAdapter
            itemAnimator = DefaultItemAnimator()
            setHasFixedSize(true)
        }

        fabRefresh.setOnClickListener { viewModel.getFruitsList() }
    }

    override fun onStart() {
        super.onStart()
        viewModel.fruitsListLiveData.observe(viewLifecycleOwner, listChangeObserver)
    }

    override fun onResume() {
        super.onResume()
        screenShownTimeInMillis = System.currentTimeMillis()
        val screenBootTime = screenShownTimeInMillis - screenCreateTimeInMillis
        GlobalScope.launch { analytics.upload(AnalyticsType.SCREEN_TIME, screenBootTime) }
    }

    private val listChangeObserver: Observer<FruitsResponse> = Observer { fruits ->
        fruits?.let { updateUI(it.fruit) } ?: run { updateUI(emptyList()) }
    }

    /**
     * Update the view on result, list of [Fruit].
     */
    private fun updateUI(list: List<Fruit>) {
        Log.d(TAG, "updateUI - list size: ${list.size}")
        when {
            list.isNotEmpty() -> {
                Log.i(TAG, "$list is Not Empty")
                tvEmptyList.visibility = View.GONE
                grpFruitsList.visibility = View.VISIBLE
            }
            else -> {
                Log.i(TAG, "$list is Empty")
                grpFruitsList.visibility = View.GONE
                tvEmptyList.visibility = View.VISIBLE
            }
        }
        mAdapter.updateAdapter(list)
    }

    override fun onStop() {
        viewModel.fruitsListLiveData.removeObserver(listChangeObserver)
        super.onStop()
    }

}