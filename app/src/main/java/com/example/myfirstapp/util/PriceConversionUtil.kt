package com.example.myfirstapp.util

import java.text.DecimalFormat

class PriceConversionUtil : IPriceConversionUtil {

    override fun getPriceInPence(price: Int): String =
        if (price > 0) DecimalFormat("0.00").format(price.times(0.01)) else ""
}