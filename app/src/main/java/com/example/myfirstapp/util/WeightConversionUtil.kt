package com.example.myfirstapp.util

import java.text.DecimalFormat

class WeightConversionUtil : IWeightConversionUtil {

    override fun getWeightInKg(weight: Int): String =
        if (weight > 0) DecimalFormat("0.00").format(weight.times(0.001)) else ""
}