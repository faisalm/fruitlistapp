package com.example.myfirstapp.util

interface IWeightConversionUtil {

    /**
     * Method to format the supplied weight(Int) to '0.00' format
     *
     * @param weight supplied to get formatted.
     * @return the formatted weight as String if positive, blank otherwise.
     */
    fun getWeightInKg(weight: Int): String
}