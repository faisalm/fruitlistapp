package com.example.myfirstapp.util

interface IPriceConversionUtil {

    /**
     * Method to format the supplied price(Int) to '0.00' format
     *
     * @param price supplied to get formatted.
     * @return the formatted price as String if positive, blank otherwise.
     */
    fun getPriceInPence(price: Int): String
}