package com.example.myfirstapp.dto

import android.os.Parcelable
import androidx.annotation.Keep
import kotlinx.android.parcel.Parcelize

@Keep
@Parcelize
data class Fruit(
    val price: Int,
    val type: String,
    val weight: Int
) : Parcelable