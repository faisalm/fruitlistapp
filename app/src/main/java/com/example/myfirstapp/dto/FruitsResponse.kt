package com.example.myfirstapp.dto

data class FruitsResponse(
    val fruit: List<Fruit>
)