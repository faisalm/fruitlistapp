package com.example.myfirstapp.util

import com.example.myfirstapp.di.utilsModule
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.test.KoinTest
import org.koin.test.inject

class WeightConversionUtilTest : KoinTest {

    private val validatorUtil: IWeightConversionUtil by inject()

    @Before
    fun setUp() {
        startKoin { modules(utilsModule) }
    }

    @Test
    fun `is 2 digit input succeeded?`() {
        val result = validatorUtil.getWeightInKg(88)
        Assert.assertEquals(result, "0.09")
    }

    @Test
    fun `is 3 digit input succeeded?`() {
        val result = validatorUtil.getWeightInKg(149)
        Assert.assertEquals(result, "0.15")
    }

    @Test
    fun `is trailing zero input succeeded?`() {
        val result = validatorUtil.getWeightInKg(300)
        Assert.assertEquals(result, "0.30")
    }

    @Test
    fun `is zero input succeeded?`() {
        val result = validatorUtil.getWeightInKg(0)
        Assert.assertEquals(result, "")
    }

    @Test
    fun `is negative input succeeded?`() {
        val result = validatorUtil.getWeightInKg(-129)
        Assert.assertEquals(result, "")
    }

    @After
    fun tearDown() {
        stopKoin()
    }
}