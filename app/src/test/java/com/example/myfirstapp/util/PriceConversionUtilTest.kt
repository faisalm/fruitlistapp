package com.example.myfirstapp.util

import com.example.myfirstapp.di.utilsModule
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.test.KoinTest
import org.koin.test.inject

class PriceConversionUtilTest : KoinTest {

    private val validatorUtil: IPriceConversionUtil by inject()

    @Before
    fun setUp() {
        startKoin { modules(utilsModule) }
    }

    @Test
    fun `is 2 digit input succeeded?`() {
        val result = validatorUtil.getPriceInPence(88)
        Assert.assertEquals(result, "0.88")
    }

    @Test
    fun `is 3 digit input succeeded?`() {
        val result = validatorUtil.getPriceInPence(149)
        Assert.assertEquals(result, "1.49")
    }

    @Test
    fun `is trailing zero input succeeded?`() {
        val result = validatorUtil.getPriceInPence(300)
        Assert.assertEquals(result, "3.00")
    }

    @Test
    fun `is zero input succeeded?`() {
        val result = validatorUtil.getPriceInPence(0)
        Assert.assertEquals(result, "")
    }

    @Test
    fun `is negative input succeeded?`() {
        val result = validatorUtil.getPriceInPence(-129)
        Assert.assertEquals(result, "")
    }

    @After
    fun tearDown() {
        stopKoin()
    }
}