package com.example.analytics.service

object Constants {

    /**
     * Usage stats are sent by issuing an http GET request to the following URL.
     */
    const val BASE_API = "https://raw.githubusercontent.com/fmtvp/recruit-test-data/master/"
}