package com.example.analytics.service

import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

interface AnalyticsService {

    /**
     * API to upload the network time taken (in ms).
     * https://raw.githubusercontent.com/fmtvp/recruit-test-data/master/stats?event=load&data=100
     *
     * @param event type.
     * @param data to be uploaded.
     */
    @GET("stats")
    fun uploadNetRequestTimeAsync(@Query("event") event: String, @Query("data") data: Long): Deferred<Any>

    /**
     * API to upload the screen time taken (in ms).
     * https://raw.githubusercontent.com/fmtvp/recruit-test-data/master/stats?event=display&data=3000
     *
     * @param event type.
     * @param data to be uploaded.
     */
    @GET("stats")
    fun uploadScreenLoadTimeAsync(@Query("event") event: String, @Query("data") data: Long): Deferred<Any>

    /**
     * API to upload the crash.
     * https://raw.githubusercontent.com/fmtvp/recruit-test-data/master/stats?event=error&data=null%20pointer%20at%20line%205
     *
     * @param event type.
     * @param data to be uploaded.
     */
    @GET("stats")
    fun uploadCrashReportAsync(@Query("event") event: String, @Query("data") data: String): Deferred<Any>
}