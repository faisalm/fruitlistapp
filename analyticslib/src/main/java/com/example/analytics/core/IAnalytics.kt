package com.example.analytics.core

interface IAnalytics {

    /**
     * Method to upload the usage data to stats server.
     *
     * @param event to be uploaded.
     * @param data of the event as [Long].
     */
    suspend fun upload(event: AnalyticsType, data: Long)

    /**
     * * Method to upload the usage data to stats server.
     *
     * @param event to be uploaded.
     * @param data of the event as [String].
     */
    suspend fun upload(event: AnalyticsType, data: String)
}