package com.example.analytics.core

enum class AnalyticsType(val type: String) {
    NETWORK_LOAD("load"),
    SCREEN_TIME("display"),
    APP_CRASH("error")
}