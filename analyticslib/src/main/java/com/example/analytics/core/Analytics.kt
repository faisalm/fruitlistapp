package com.example.analytics.core

import com.example.analytics.core.AnalyticsType.*
import com.example.analytics.service.RetrofitFactory

@Suppress("DeferredResultUnused")
class Analytics : IAnalytics {

    private val analyticsAPIService = RetrofitFactory.create()

    override suspend fun upload(event: AnalyticsType, data: Long) {
        when (event) {
            NETWORK_LOAD -> analyticsAPIService.uploadNetRequestTimeAsync(NETWORK_LOAD.type, data)
            SCREEN_TIME -> analyticsAPIService.uploadScreenLoadTimeAsync(SCREEN_TIME.type, data)
            else -> TODO()
        }
    }

    override suspend fun upload(event: AnalyticsType, data: String) {
        analyticsAPIService.uploadCrashReportAsync(APP_CRASH.type, data)
    }
}