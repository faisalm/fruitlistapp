# Introduction [![pipeline status](https://gitlab.com/faisalm/fruitlistapp/badges/master/pipeline.svg)](https://gitlab.com/faisalm/fruitlistapp/commits/master)

The FruitApp is an Android native appl consisting of two screens for displaying data about fruit. 
The following specification must be met:

The first screen will display a list of fruit.
When the user selects an item of fruit the application should show a second screen containing more information about the fruit, specifically
   
- Price in pounds and pence
- Weight in KG

This is a template for doing Android development using GitLab and [fastlane](https://fastlane.tools/).

## What's contained in this project

### Prerequisites

- AndroidStudio 3.6
- Java JDK 1.8
- Kotlin 1.3.40
- Android SDK minimum API level 22

### Libraries used

- [RetroFit](https://square.github.io/retrofit/) - A REST Client library (Helper Library), Type-safe HTTP client for Android and used in Android and Koltin to create an HTTP request and also to process the HTTP response from a REST API.
- [KOIN](https://github.com/InsertKoinIO/koin) - a pragmatic lightweight dependency injection framework for Kotlin.
- [OkHttp interceptor](https://square.github.io/okhttp/interceptors/) - logs HTTP request and response data.
- Kotlin [Coroutines](https://developer.android.com/kotlin/coroutines) - Concurrency design pattern that used in Android to simplify code that executes asynchronously. 
- [MOSHI](https://github.com/square/moshi) - A modern JSON library for Android and Java. It makes it easy to parse JSON into Java objects.
- [Coil](https://github.com/coil-kt/coil) - Image loading for Android backed by Kotlin Coroutines.

### Android components

- RecyclerView - Used to list the Fruits from remote network.
- Constraintlayout - Simple, flat hierarchies in a layout.
- Synthetic Binding(Kotlin) - bind the data with UI, wonder not if findViewById() missing in any UI.

### [Android Architecture Components](https://developer.android.com/topic/libraries/architecture/)([Android Jetpack](https://developer.android.com/jetpack/))

- [Material Theme](https://material.io/develop/android/docs/getting-started/) - Material design is a comprehensive guide for visual, motion, and interaction design across platforms and devices.
- [AndroidX](https://developer.android.com/jetpack/androidx/) - Complete project implemented using AndroidX libraries.
- [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel) - allows data to survive configuration changes such as screen rotations.
- [LiveData](https://developer.android.com/topic/libraries/architecture/livedata) - lifecycle-aware data holder class to respects the lifecycle Fragments.
- [Navigation](https://developer.android.com/guide/navigation/) - Android Jetpack's Navigation component helps to implement navigation, from simple button clicks to more complex patterns.

### Workflow of the application

The app name is **FruitApp**. 
Once installed it can be found in the App tray with a **fruit(apple, pear)** icon.

### Implementation

1. The App is designed to the list of fruits from the [network](https://raw.githubusercontent.com/fmtvp/recruit-test-data/master/) by calling intended API endpoint. The UI implemented as LinearLayout(RecycleView) to showcase the resulting list.
2. The network call is performed using Retrofit2 in conjunction with Kotlin Coroutines and expects to convert the results to be mapped onto Kotlin data classes using Moshi converter. RxJava helps to retrieve the result asynchronously and handover the result to Moshi. for further processing.
3. ViewModel in combination with LiveData helps the data to survive view lifecycle changes. Moreover, maintains an abstraction between the View and ViewModel.
4. Used Koin as dependency injection to have smooth integration with Kotlin, The complete code written in Kotlin.
5. Implemented Analytics module (Usage stats) as a separate module from the core app to showcase the module-based approach in Android application development.

### Design

- The application is based on the MVVM pattern. In MVVM architecture, Views react to changes in the ViewModel without being called explicitly.
- The API requests are made using retrofit.
- The Moshi is used conjunction retrofit to parser results in Kotlin data classes.
- The ViewModel interacts with a data repository(Using Retrofit) to get the data and updates the View.
- The data source manages the data to be fetched from the network on UI scrolls using paging library.
- The ViewModel delegates all the requests from the view to the repository and vice versa.
- The RecylerView is used instead of the normal list view.
- The Kotlin Coroutines takes care of asynchronous patterns wherever implemented.
- The Navigation component manages the flow between the Views.

### Further enhancements

- The stage-based result on No data/ limited network scenario to handled with progress bar.
- Data optimisation using best practices.
- furthermore, the refactoring is an endless dream.

### Fastlane files

It also has Fastlane setup per GitLab's [blog post](https://about.gitlab.com/2019/01/28/android-publishing-with-gitlab-and-fastlane/) on
getting GitLab CI set up with Fastlane. 

### Dockerfile build environment

In the root, there is a Dockerfile which defines a build environment which will be
used to ensure consistent and reliable builds of the Android application using
the correct Android SDK and other details expected.

We generate this environment as needed because installing the Android SDK
for every pipeline run would be very slow.

### Gradle configuration

The Gradle configuration is exactly as output by Android Studio except for the version name being updated to 

It is set as following keep the auto-versioning as per commit SHA-1s.

`versionName "1.0-${System.env.VERSION_SHA}"`
